#!/bin/zsh
cd "$(dirname "$(readlink -f "$0")")"
source config.inc

list_all_paths() {
  eval "${MPC} -f '%file%' playlist ${PLIST}"
}

filter_actual_files() { # stdin = files
  while read line
    do [ -f "${MUSIC_DIR}/${line}" ] && echo "${line}"
  done
}

find_without_sticker() { # stdin = files
  while read line
    do eval "${MPC} sticker \"${line}\" get ${STICKER}" >/dev/null 2>&1 || echo "${line}"
  done
}

get_one_bpm() { # $1 = file
  rm -f /tmp/ss.wav
  sox --norm=-0.2 "${MUSIC_DIR}/${1}" -t wav -e signed-integer /tmp/ss.wav
  rate="$(soundstretch /tmp/ss.wav -bpm 2>&1 | grep "Detected BPM rate" | cut -d" " -f 4)"
  if [ "$rate" = "" ]
    then rate="<fail>"
  fi
  echo "$rate"
}

set_bpm_stickers() { # stdin = files
  while read line
    do echo "$line"
    bpm="$(get_one_bpm "$line")"
    echo "$bpm"
    eval "${MPC} sticker \"${line}\" set ${STICKER} \"${bpm}\""
  done
}

list_all_paths | filter_actual_files | find_without_sticker | set_bpm_stickers
