#!/bin/zsh
cd "$(dirname "$(readlink -f "$0")")"
source config.inc

playlist_length() {
  eval "${MPC} playlist | wc -l"
}

cur_song() {
  eval "${MPC} -f '%file%' current"
}

avg_bpm() {
  sum=0
  num=0
  eval "${MPC} -f '%file%' playlist" | while read file
    do val=$(eval "${MPC} sticker \"${file}\" get ${STICKER}" | grep -v "<fail>" | cut -d= -f2)
    [ "x$val" = "x" ] && continue
    sum="$[ $sum + $val ]"
    num="$[ $num + 1 ]"
  done
  echo "$[ $sum / $num ]"
}

should_refill() {
  [ $(playlist_length) -lt $1 ] && [ ! "x$(avg_bpm)" = "x" ]
}

queue_song() { # $1 = file
  echo "${1}"
  eval "${MPC} -f '%file%' playlist" | grep -q "${1}" && echo "dupe!" || eval "${MPC} add \"${1}\""
}

abs() {
  val="$[ $@ ]"
  [[ $val -lt 0 ]] && echo "$[ $val * -1 ]" || echo "$val"
}

make_list() {
  avg="$(avg_bpm)"
  eval "${MPC} -f '%file%' playlist ${PLIST}" | while read file
    do bpm="$(eval "${MPC} sticker \"${file}\" get ${STICKER}" 2>/dev/null | cut -d= -f2)"
    [ "$bpm" = "<fail>" -o "x$bpm" = "x" ] && continue
    delta=$(abs $bpm - $avg)
    echo "$[ $delta * $delta ] $file"
  done | sort -n
}

pick_one() {
  shuf -n 1 | cut -d' ' -f2-
}

prev_song=""
eval "${MPC} idleloop" | while read reason
  do cur="$(cur_song)"
  [ "$cur" = "$prev_song" ] && continue
  prev_song="$cur"
  should_refill $WATERMARKL || continue
  echo "working..."
  make_list > /dev/shm/msp.list
  len=$(wc -l < /dev/shm/msp.list)
  head -n $[ $len / $TIGHT ] < /dev/shm/msp.list > /dev/shm/msp.hlist
  while should_refill $WATERMARKH
    do # zsh has a bug where $RANDOM returns the same in a function if not used globally
    queue_song "$(pick_one < /dev/shm/msp.hlist)"
  done
  while read -t 0 discard ; do :; done # clear stdin
done

