# mpd-smart-playlist

## `config.inc`

* `MPC` command prefix to make `mpc` connect
* `MUSIC_DIR` your music dir
* `STICKER` name of the sticker to use for bpm
* `PLIST` name of the playlist to use as source
* `WATERMARKL` how many songs left in queue before refilling
* `WATERMARKH` refill to how many songs
* `TIGHT` the most fitting `total number of songs / $TIGHT` songs are picked from

## `update_bpm.zsh`

this refills the `$STICKER` on each song in `$PLIST` with the BPM of that song, if possible.

*note:* the measured BPM can be rather inaccurate (i had it report a delta of 80 between two copies of the same song in different encodings, and for a lot it refuses to even measure), if you disagree with the result simply manually change `$STICKER` on the song in question, it'll skip any known ones on update

## `refill.zsh`

this loops (smartly; does nothing most of the time) and refills the play queue with "fitting" songs

*note:* i would like to actually make the pick a weighted-random, but because my brain was overloading i settled for "take a random pick from the first `$TIGHT`th"
